<script src="view/js/jQuery.js"></script>
<script src="view/js/jQuery-mask.js"></script>
<script src="view/js/jMoneyMask.js"></script>
<script src="view/js/datatables.js"></script>
<script src="view/js/datatables-buttons.js"></script>
<script src="view/js/datatables-buttons-flash.js"></script>
<script src="view/js/jszip.js"></script>
<script src="view/js/pdfmake.js"></script>
<script src="view/js/vfs-fonts.js"></script>
<script src="view/js/buttons-html5.min.js"></script>
<script src="view/js/buttons-print.min.js"></script>
<script src="view/js/bootstrap/bootstrap.js"></script>
<script src="view/js/errorHandler.js"></script>
<script src="view/js/httpHandler.js"></script>
<link rel="stylesheet" href="view/css/bootstrap/bootstrap.css">
<link rel="stylesheet" href="view/css/fontawesome/css/all.css">
<link rel="stylesheet" href="view/css/style.css">
<link rel="stylesheet" href="view/css/datatables.css">
<?php
include "vendor/autoload.php";

$file_path = 'view/public/';

$REQUEST_URI =  filter_input(INPUT_SERVER, 'REQUEST_URI');
$INIT = strpos($REQUEST_URI, '?');
if ($INIT) 
    $REQUEST_URI = substr($REQUEST_URI, 0, $INIT);

$DIR = substr($REQUEST_URI, 1);
$URI = explode('/', $DIR);
$count = 0;
if ($URI[0] == 'HickSystem')
    $count = 1;
if ($URI[$count] == '')
    $URI[$count] = 'login';

if (file_exists($file_path . "/" . $URI[$count] . ".php")){
    require ($file_path . "/" . $URI[$count] . ".php");
}elseif (is_dir($file_path . "/" . $URI[$count])){
    if (isset($URI[$count]) && file_exists($file_path . "/" . $URI[$count] . "/" .$URI[$count+1] . ".php")){
        require ($file_path . "/" . $URI[$count] . "/" .$URI[$count+1] . ".php");
    }
}

if (file_exists($file_path . "/" . $URI[$count] . ".html")){
    require($file_path . "/" . $URI[$count] . ".html");
}elseif (is_dir($file_path . "/" . $URI[$count])){
    if (isset($URI[$count]) && file_exists($file_path . "/" . $URI[$count] . "/" .$URI[$count+1] . ".html")){
        require ($file_path . "/" . $URI[$count] . "/" .$URI[$count+1] . ".html");
    }
}


