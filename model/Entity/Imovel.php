<?php

namespace Model\Entity;

class Imovel extends \GORM\Model{

    public $endereco;
    public $bairro;
    public $complemento;
    public $referencia;
    public $cep;
    public $status;
    public $sabesp;
    public $elektro;
    public $iptu;
    public $cidade_id;

    public function beforeSave(){
        $this->cep = str_replace('-','', $this->cep);
        $this->elektro = ($this->elektro) ? '1' : '0';
        $this->sabesp = ($this->sabesp) ? '1' : '0';
        $this->iptu = ($this->iptu) ? '1' : '0';
        
    }
    public function beforeUpdate(){
        $this->cep = str_replace('-','', $this->cep);
        $this->elektro = ($this->elektro == "true") ? '1' : '0';
        $this->sabesp = ($this->sabesp == "true") ? '1' : '0';
        $this->iptu = ($this->iptu == "true") ? '1' : '0';
    }

}