<?php

namespace Model\Entity;
use DateTime;
class Inquilino extends \GORM\Model{

    public $id;
    public $nomeCompleto;
    public $cpf;
    public $rg;
    public $orgaoEmissor;
    public $dataNascimento;
    public $nomeConjuje;
    public $telefoneFixo;
    public $telefoneCelular;
    public $estadoCivil;
    public $email;
    public $email2;
    public $imovel_id;

    function beforeSave(){
        $date = explode("/",$this->dataNascimento);
        $this->dataNascimento = new DateTime($date[2]."-".$date[1]."-".$date[0]);
        $this->dataNascimento = $this->dataNascimento->format("Y-m-d");
    }
    function beforeUpdate(){
        $date = explode("/",$this->dataNascimento);
        $this->dataNascimento = new DateTime($date[2]."-".$date[1]."-".$date[0]);
        $this->dataNascimento = $this->dataNascimento->format("Y-m-d");
    }
    function afterSelect(&$cls){
        $cls->dataNascimento = new DateTime($cls->dataNascimento);
        $cls->dataNascimento = $cls->dataNascimento->format("d/m/Y");
    }
}