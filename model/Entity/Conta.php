<?php

namespace Model\Entity;
use DateTime;
class Conta extends \GORM\Model{

    public $descricao;
    public $valor;
    public $dataVencimento;
    public $dataPagamento;
    public $tipo_id;
    public $status;
    public $imovel_id;

    function beforeSave(){
        if ($this->valor != ""){
            $this->valor = str_replace('.','', $this->valor);
            $this->valor = (float) str_replace(',','.', $this->valor);
        }
        if ($this->dataPagamento != ""){
            
            $date = explode("/",$this->dataPagamento);
            $this->dataPagamento = new DateTime($date[2]."-".$date[1]."-".$date[0]);
            $this->dataPagamento = $this->dataPagamento->format("Y-m-d");
        }
        if($this->dataVencimento != ""){

            $date = explode("/",$this->dataVencimento);   
            $this->dataVencimento = new DateTime($date[2]."-".$date[1]."-".$date[0]);
            $this->dataVencimento = $this->dataVencimento->format("Y-m-d");
        }

    }
    function beforeUpdate(){
        if ($this->valor != ""){
            $this->valor = (float) str_replace(',','.', $this->valor);
        }
        if ($this->dataPagamento != ""){
            
            $date = explode("/",$this->dataPagamento);
            $this->dataPagamento = new DateTime($date[2]."-".$date[1]."-".$date[0]);
            $this->dataPagamento = $this->dataPagamento->format("Y-m-d");
        }
        if($this->dataVencimento != ""){

            $date = explode("/",$this->dataVencimento);   
            $this->dataVencimento = new DateTime($date[2]."-".$date[1]."-".$date[0]);
            $this->dataVencimento = $this->dataVencimento->format("Y-m-d");
        }
    }
    function afterSelect(&$cls){
        $cls->dataPagamento = new DateTime($cls->dataPagamento);
        $cls->dataPagamento = $cls->dataPagamento->format("d/m/Y");
        $cls->dataVencimento = new DateTime($cls->dataVencimento);
        $cls->dataVencimento = $cls->dataVencimento->format("d/m/Y");
    }
}