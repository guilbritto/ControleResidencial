<?php

namespace Model\Entity;

class Estado extends \GORM\Model{
    public $id;
    public $nome;
    public $sigla;
    public $pais;
}