<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Model\Entity\Usuario;
use \Model\Entity\Estado;
use \Model\Entity\Cidade;
use \Model\Entity\Imovel;
use \Model\Entity\Inquilino;
use \Model\Entity\Conta;
use \Model\Entity\Tipo;

require '../vendor/autoload.php';


$app = new \Slim\App;
/**
 * relatorio de view
 */
$app->get('/main', function ( $request,  $response,  $args) {
    $imoveis = Imovel::getInstance();
    $sql = "SELECT 
            (SELECT count(*) FROM imovel where status like '%Alugado%') AS ALUGADO, 
            (SELECT count(*) FROM imovel where status like '%Vago%') AS VAGO, 
            (SELECT count(*) FROM imovel where status like '%Outros%') AS OUTROS, 
            (SELECT count(*) FROM imovel ) AS TODOS 
            FROM DUAL";
    $collection = $imoveis->execute($sql, true);
    if ($collection)    
        return $response->withJson($collection);
    else   
        return $response->withJson(false);
});
/**
 * Loga na Aplicação
 */
$app->post('/login', function ( $request,  $response,  $args) {
    $post = $request->getParsedBody();
    $usuario = Usuario::getInstance();
    $collection = $usuario->makeSelect()->where('login="'.$post['login'].'"')->_and("senha='".$post['senha']."'")->execute();
    if ($collection->exists(0))    
        return $response->withJson($collection->get(0));
    else   
        return $response->withJson(false);
});
/***************************************************************
 * *************************************************************
 *                            IMOVEL              
 * *************************************************************
 **************************************************************/
/**
 * Retorna todos os estados cadastrados
 */
$app->get('/estado', function ( $request,  $response,  $args) {
    $post = $request->getParsedBody();
    $estado = Estado::getInstance();
    $collection = $estado->makeSelect()->execute();
    if ($collection->exists(0))    
        return $response->withJson($collection->getAll());
    else   
        return $response->withJson(false);
});
/**
 * retorna todas as cidades cadastradas
 */
$app->get('/cidade', function ( $request,  $response,  $args) {
    $cidade = Cidade::getInstance();
    $collection = $cidade->makeSelect()->execute();
    if ($collection->exists(0))    
        return $response->withJson($collection->getAll());
    else   
        return $response->withJson(false);
});
/**
 * Retorna todas as cidades de um estado
 */
$app->get('/estadoscidades/{id}', function ( $request,  $response,  $args) {
    $cidade = Cidade::getInstance();
    $collection = $cidade->makeSelect()->where("estado=".$args['id'])->execute();
    
    if ($collection->exists(0))    
        return $response->withJson($collection->getAll());
    else   
        return $response->withJson(false);
});
/**
 * Retorna todas as cidades de um estado
 */
$app->post('/imovel', function ( $request,  $response,  $args) {
    $post = $request->getParsedBody();
    $imovel = Imovel::getInstance();
    $imovel->load($post);
    $r = $imovel->save();
    if($r['flag'])
        return $response->withJson(true);
    else   
        return $response->withJson(false); 
});
/**
 * Retorna todas as cidades de um estado
 */
$app->put('/imovel/{id}', function ( $request,  $response,  $args) {
    $post = $request->getParsedBody();
    $imovel = Imovel::getInstance();
    $imovel->load($post);
    $r = $imovel->update();
    if($r['flag'])
        return $response->withJson(true);
    else   
        return $response->withJson(false); 
});
/**
 * Retorna todos os imoveis cadastrados no sistema
 */
$app->get('/imovel', function ( $request,  $response,  $args) {
    $imoveis = Imovel::getInstance();
    $sql = "SELECT i.id, i.cod , i.endereco,  i.bairro, i.cidade_id,  c.nome as cidade, e.nome as estado, e.id as estado_id, i.cep, i.status 
            FROM imovel i, cidade c, estado e 
            WHERE i.cidade_id=c.id 
            AND c.estado=e.id";
    $collection = $imoveis->execute($sql);
    if ($collection->exists(0))    
        return $response->withJson($collection->getAll());
    else   
        return $response->withJson(false);
});
/**
 * Retorna todos os imoveis cadastrados no sistema
 */
$app->get('/imovel/{id}', function ( $request,  $response,  $args) {
    $imoveis = Imovel::getInstance();
    $sql = "SELECT i.id, i.cod , i.endereco,  i.bairro, i.cidade_id, e.id as estado_id, i.sabesp,i.elektro, i.iptu, i.referencia, i.complemento, i.cep, i.status, 
    c.nome as cidade, e.id as estado,  c.id as cidadeId, e.nome as estadoNome
            FROM imovel i, cidade c, estado e 
            WHERE i.cidade_id=c.id 
            AND c.estado=e.id
            AND i.id=".$args["id"];
    $collection = $imoveis->execute($sql);
    if ($collection->exists(0))    
        return $response->withJson($collection->getAll());
    else   
        return $response->withJson(false);
});
/**
 * Retorna todos os imoveis cadastrados no sistema
 */
$app->delete('/imovel/{id}', function ( $request,  $response,  $args) {
    $imovel = Imovel::getInstance();
    $imovel->id = $args["id"];
    if ($imovel->delete())
        return $response->withJson(true);
    else   
        return $response->withJson(false);
});
/***************************************************************
 * *************************************************************
 *                            INQUILINO              
 * *************************************************************
 **************************************************************/
/**
 * Salva Inquilinos
 */
$app->post('/inquilino', function ( $request,  $response,  $args) {
    $post = $request->getParsedBody();
    $inquilino = Inquilino::getInstance();
    $inquilino->load($post);
    $r = $inquilino->save();
    if($r['flag'])
        return $response->withJson(true);
    else   
        return $response->withJson(false); 
});
/**
 * Lista inquilinos
 */
$app->get('/inquilino', function ( $request,  $response,  $args) {
    $inquilino = Inquilino::getInstance();
    $collection = $inquilino->makeSelect("inquilino.*, imovel.endereco as imovel_id")->inner("imovel", "inquilino.imovel_id=imovel.id")->execute();
   
    if ($collection->exists(0))
        return $response->withJson($collection->getAll());
    else   
        return $response->withJson(false);
});
/**
 * Lista inquilinos por ids
 */
$app->get('/inquilino/{id}', function ( $request,  $response,  $args) {
    $inquilino = Inquilino::getInstance();
    $collection = $inquilino->makeSelect("inquilino.*, imovel.endereco as imovel")->inner("imovel", "inquilino.imovel_id=imovel.id")
    ->where("inquilino.id = ".$args['id'])->execute();
    if ($collection->exists(0))
        return $response->withJson($collection->getAll());
    else   
        return $response->withJson(false);
 });
/**
 * Edita inquilinos
 */
$app->put('/inquilino/{id}', function ( $request,  $response,  $args) {
    $post = $request->getParsedBody();
    $inquilino = Inquilino::getInstance();
    $inquilino->load($post);
    $inquilino->id = $args['id'];
    $r = $inquilino->update();
    if($r['flag'])
        return $response->withJson(true);
    else   
        return $response->withJson(false); 
 });
/**
 * Deleta inquilinos
 */
$app->delete('/inquilino/{id}', function ( $request,  $response,  $args) {
    $inquilino = Inquilino::getInstance();
    $inquilino->id = $args['id'];
    $r = $inquilino->delete();
    if($r['flag'])
        return $response->withJson(true);
    else   
        return $response->withJson(false); 
 });
 /***************************************************************
 * *************************************************************
 *                            Contas              
 * *************************************************************
 **************************************************************/
/**
 * Salva Contas
 */
$app->post('/conta', function ( $request,  $response,  $args) {
    $post = $request->getParsedBody();
    $conta = Conta::getInstance();
    $conta->load($post);
    $r = $conta->save();
    if($r['flag'])
        return $response->withJson(true);
    else   
        return $response->withJson(false); 
});
/**
 * Lista Contas
 */
$app->get('/conta', function ( $request,  $response,  $args) {
    $conta = Conta::getInstance();
    $collection = $conta->makeSelect("conta.*, imovel.cod as imovel_id, tipo.nome as tipo_id")->inner("imovel", "conta.imovel_id=imovel.id")
    ->inner("tipo", "conta.tipo_id=tipo.id")->execute();
    if ($collection->exists(0))
        return $response->withJson($collection->getAll());
    else   
        return $response->withJson(false);
});
/**
 * Lista Contas por ids
 */
$app->get('/conta/{id}', function ( $request,  $response,  $args) {
    $conta = Conta::getInstance();
    $collection = $conta->makeSelect("conta.*, imovel.cod as imovel, tipo.nome as tipo")->inner("imovel", "conta.imovel_id=imovel.id")->inner("tipo", "conta.tipo_id=tipo.id")
    ->where("conta.id = ".$args['id'])->execute();
    if ($collection->exists(0))
        return $response->withJson($collection->getAll());
    else   
        return $response->withJson(false);
 });
/**
 * Edita Contas
 */
$app->put('/conta/{id}', function ( $request,  $response,  $args) {
    $post = $request->getParsedBody();
    $conta = Conta::getInstance();
    $conta->load($post);
    $conta->id = $args['id'];
    $r = $conta->update();
    if($r['flag'])
        return $response->withJson(true);
    else   
        return $response->withJson(false); 
 });
/**
 * Deleta Contas
 */
$app->delete('/conta/{id}', function ( $request,  $response,  $args) {
    $conta = Conta::getInstance();
    $conta->id = $args['id'];
    $r = $conta->delete();
    if($r['flag'])
        return $response->withJson(true);
    else   
        return $response->withJson(false); 
 });
 /**
 * Lista Contas por ids
 */
$app->get('/contas/pendentes', function ( $request,  $response,  $args) {
    $conta = Conta::getInstance();
    $sql = 'SELECT conta.*, imovel.cod cod FROM conta, imovel WHERE DATE_SUB(dataVencimento, INTERVAL 5 day) <= NOW() AND conta.status = "PENDENTE" AND imovel.id = conta.imovel_id ORDER BY DataVencimento
    ';
    $collection = $conta->execute($sql);
    if ($collection->exists(0))    
        return $response->withJson($collection->getAll());
    else   
        return $response->withJson(false);
 });

 /***************************************************************
 * *************************************************************
 *                            Tipo              
 * *************************************************************
 **************************************************************/
/**
 * Salva Tipos
 */
$app->post('/tipo', function ( $request,  $response,  $args) {
    $post = $request->getParsedBody();
    $tipo = Tipo::getInstance();
    $tipo->load($post);
    $r = $tipo->save();
    if($r['flag'])
        return $response->withJson(true);
    else   
        return $response->withJson(false); 
});
/**
 * Lista Tipos
 */
$app->get('/tipo', function ( $request,  $response,  $args) {
    $tipo = Tipo::getInstance();
    $collection = $tipo->makeSelect()->execute();
   
    if ($collection->exists(0))
        return $response->withJson($collection->getAll());
    else   
        return $response->withJson(false);
});
/**
 * Lista Tipos por ids
 */
$app->get('/tipo/{id}', function ( $request,  $response,  $args) {
    $tipo = Tipo::getInstance();
    $collection = $tipo->makeSelect()->where("id = ".$args['id'])->execute();
    if ($collection->exists(0))
        return $response->withJson($collection->getAll());
    else   
        return $response->withJson(false);
 });
/**
 * Edita Tipos
 */
$app->put('/tipo/{id}', function ( $request,  $response,  $args) {
    $post = $request->getParsedBody();
    $tipo = Tipo::getInstance();
    $tipo->load($post);
    $tipo->id = $args['id'];
    $r = $tipo->update();
    if($r['flag'])
        return $response->withJson(true);
    else   
        return $response->withJson(false); 
 });
/**
 * Deleta Tipos
 */
$app->delete('/tipo/{id}', function ( $request,  $response,  $args) {
    $tipo = Tipo::getInstance();
    $tipo->id = $args['id'];
    $r = $tipo->delete();
    if($r['flag'])
        return $response->withJson(true);
    else   
        return $response->withJson(["message" => "Não é possivel excluir um tipo com uma conta vinculada", "falg" => false]);
 });
$app->run();