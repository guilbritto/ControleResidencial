/**
 * Metodo para fazer requisições POST, DELETE, PUT
 * dependendo do tipo passado por parametro
 * @param {Url que será feita a requisição} url 
 * @param {Array Json com as informções enviadas para o servidor} data 
 * @param {Tipo da requisição quer será feita para o servidor por default ele utiliza POST} type 
 */
function post(url, data = NaN, type = "POST"){
    var resp = $.ajax({
        url: url,
        type: type,
        data: data,
        dataType: 'json'
    });
    return resp;
}
/**
 * Função para poder fazer uma requisição GET para o servidor de destino
 * @param {String Url de conexão com servidor REST} url 
 */
function get(url){
    $.get( url, function( response ) {
       return response
    });
}
