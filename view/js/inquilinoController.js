


var consulta = false;
function fillImoveis(){
    $.get( "controller/app.php/imovel", function( response ) {
        var selectbox = $("#imovel_id");
        $.each(response, function (i, d) {
            $('<option>').val(d.id).text(d.endereco).appendTo(selectbox);
        });
    });
}
function showConjuje(){
    console.log($("#estadoCivil").val());
    if($("#estadoCivil").val() == 1){
        $("#divConjuje").show();
    }
    else{
        $("#divConjuje").hide();
    }
}
/**
 * Função para salvar os imoveis
 */
function salvar(){
    var data = {
        nomeCompleto: $("#nomeCompleto").val(),
        cpf: $("#cpf").val(),
        rg: $("#rg").val(),
        estadoCivil: $("#estadoCivil").val(),
        orgaoEmissor: $("#orgaoEmissor").val(),
        dataNascimento: $("#dataNascimento").val(),
        nomeConjuje: $("#nomeConjuje").val(),
        telefoneFixo: $("#telefoneFixo").val(),
        telefoneCelular: $("#telefoneCelular").val(),
        email: $("#email").val(),
        email2: $("#email2").val(),
        imovel_id: $("#imovel_id option:selected").val()
    }
    if (data.nomeComplmento == '')
        showMessage("alertas","É necessário informar o nome completo do inquilino", "danger" );
    if (data.cpf == '')
        showMessage("alertas","É necessário informar o CPF do inquilino", "danger" );
    if (data.rg == '')
        showMessage("alertas","É necessário informar o RG do cliente", "danger" );
    if (data.imovel_id == -1)
        showMessage("alertas","É necessário informar um imóvel para o inquilino", "danger" );
    if (data.dataNascimento == '')
        showMessage("alertas","É necessário informar a data de nascimento do inquilino", "danger" );
    if (data.email == '')
        showMessage("alertas","É necessário informar pelo menos um email", "danger" );
    if (data.telefoneCelular == '')
        showMessage("alertas","É necessário informar um telefone Celular", "danger" );
    $.ajax({
        url: "controller/app.php/inquilino",
        type: "POST",
        data: data,
        dataType: 'json'
    }).done(function(response){
        if(response)
            showMessage("alertas","Cadastro efetuado com sucesso!", "success" );
    }); 

} 
function getInquilino(){
    $("#dtBody").html("");
    $.get("controller/app.php/inquilino", function( response ) {
        $.each(response, function (key, value) {
            $("#dtBody")
            .append(
                "<tr>\
                    <td>"+value.nomeCompleto+"</td>\
                    <td>"+value.cpf+"</td>\
                    <td>"+value.rg+"</td>\
                    <td>"+value.imovel_id+"</td>\
                    <td>NULL</td>\
                    <td>"+value.telefoneFixo+"</td>\
                    <td>"+value.telefoneCelular+"</td>\
                    <td><a href='#' onclick='excluirInquilino("+value.id+")' data-toggle='modal' data-target='#modal'>\
                            <i class='fas fa-trash-alt'></i>\
                         </a> \
                         <a href='#' onclick='editarInquilino("+value.id+")' data-toggle='modal' data-target='#modalEdit'> \
                            <i class='fas fa-edit'></i>\
                        </a>\
                    </td>\
                </tr>"
            );
        });
        $("#inquilino").DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'print',
                    messageTop: "Relatório de Inquilinos",
                    exportOptions:{
                        columns: [0,1,2,3,4,5,6]
                    }
                },
                {
                    extend: 'excelHtml5',
                    messageTop: "Relatório de Inquilinos",
                    exportOptions:{
                        columns: [0,1,2,3,4,5,6]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    messageTop: "Relatório de Inquilinos",
                    exportOptions:{
                        columns: [0,1,2,3,4,5,6]
                    }
                }
            ],
            language:{
                url : "view/js/translate.json"
            }    
        });
    });
}

function excluirInquilino(id){
    $("#modalEditLabel").text("Exlusão de Registro");
    $("#modalBody").html("<p>Voce realmente deseja excluir este registro? </p>");
    $("#modalFooter")
    .html('<button type="button" class="btn btn-outline-dark" onclick="exclui('+id+')">Sim</button> <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Não</button>');
    $("#modal").modal("show");
   
}
function exclui(id){
    $.ajax({
        url: "controller/app.php/inquilino/"+id,
        type: "DELETE",
        dataType: 'json'
    }).done(function(response){
        if(response)
            showMessage("alertas","Deleção efetuada com sucesso", "success" );
    }); 
    $("#modal").modal("hide");
    getInquilino();
}
function editarInquilino(id){
    
    $("#modalFooter").html("");
    $("#modalContent").css("width", "850px");
    $("#modalEditLabel").text("Editar Registro");
    $("#modalBody").load('view/public/cadInquilino.html');
    $.get("controller/app.php/inquilino/"+id, function( response ) {
        $.each(response, function (key, value) {
            $("#nomeCompleto").val(value.nomeCompleto);
            $("#cpf").val(value.cpf);
            $("#rg").val(value.rg);
            $("#estadoCivil").val(value.estadoCivil);
            $("#orgaoEmissor").val(value.orgaoEmissor);
            $("#dataNascimento").val(value.dataNascimento);
            $("#telefoneFixo").val(value.telefoneFixo);
            $("#telefoneCelular").val(value.telefoneCelular);
            $("#estadoCivil").val(value.estadoCivil);
            $("#email").val(value.email);
            $("#email2").val(value.email2);
            $("#nomeConjuje").val(value.nomeConjuje);
            $("#imovel_id option").val(value.imovel_id).text(value.imovel);
            $("#btnSaveEdit").attr("onClick", "Editar("+value.id+")");
        });
        $("#h1Cad").remove();
        $("#hrCad").remove();
    });
    $("#modal").modal("show");
}
function Editar(id){
    var data = {
        nomeCompleto: $("#nomeCompleto").val(),
        cpf: $("#cpf").val(),
        rg: $("#rg").val(),
        estadoCivil: $("#estadoCivil").val(),
        orgaoEmissor: $("#orgaoEmissor").val(),
        dataNascimento: $("#dataNascimento").val(),
        nomeConjuje: $("#nomeConjuje").val(),
        telefoneFixo: $("#telefoneFixo").val(),
        telefoneCelular: $("#telefoneCelular").val(),
        email: $("#email").val(),
        email2: $("#email2").val(),
        imovel_id: $("#imovel_id option:selected").val()
    }
    if (data.nomeComplmento == '')
        showMessage("alertas","É necessário informar o nome completo do inquilino", "danger" );
    if (data.cpf == '')
        showMessage("alertas","É necessário informar o CPF do inquilino", "danger" );
    if (data.rg == '')
        showMessage("alertas","É necessário informar o RG do cliente", "danger" );
    if (data.imovel_id == -1)
        showMessage("alertas","É necessário informar um imóvel para o inquilino", "danger" );
    if (data.dataNascimento == '')
        showMessage("alertas","É necessário informar a data de nascimento do inquilino", "danger" );
    if (data.email == '')
        showMessage("alertas","É necessário informar pelo menos um email", "danger" );
    $.ajax({
        url: "controller/app.php/inquilino/"+id,
        type: "PUT",
        data: data,
        dataType: 'json'
    }).done(function(response){
        if(response)
            showMessage("alertas","Cadastro efetuado com sucesso!", "success" );
    }); 

    $("#modal").modal("hide");
    getInquilino();
}
