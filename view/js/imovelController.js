
$.ajax({
    url: "controller/app.php/estado",
    type: "GET",
    async: false
}).done(function(response) {
    var selectbox = $("#estado");
    $.each(response, function (i, d) {
        $('<option>').val(d.id).text(d.nome).appendTo(selectbox);
    });
});
/**
 * Função para preencher as cidades
 */
function fillCidade(){
    $.ajax({
        url: "controller/app.php/estadoscidades/"+$("#estado").val(),
        type: "GET",
        async: false
    }).done(function(response) {
        var selectbox = $("#cidade");
        selectbox.find('option').remove();
        $.each(response, function (i, d) {
            $('<option>').val(d.id).text(d.nome).appendTo(selectbox);
        });
    });
}
/**
 * Função para salvar os imoveis
 */
function salvar(){
    var data = {
        cod: $("#cod").val(),
        endereco: $("#endereco").val(),
        bairro: $("#bairro").val(),
        cidade_id: $("#cidade").val(),
        cep: $("#cep").val(),
        complemento: $("#complemento").val(),
        referencia: $("#referencia").val(),
        status: $("#status option:selected").text(),
        sabesp: $("#sabesp").is(":checked"),
        elektro: $("#elektro").is(":checked"),
        iptu: $("#iptu").is(":checked")
    }
    if (data.cod == '')
        showMessage("alertas","É necessário informar o campo Cod", "danger" );
    if (data.endereco == '')
        showMessage("alertas","É necessário informar o campo endereco", "danger" );
    if (data.bairro == '')
        showMessage("alertas","É necessário informar o campo bairro", "danger" );
    if (data.cidade == -1)
        showMessage("alertas","É necessário informar o campo cidade", "danger" );
    if (data.cep == '')
        showMessage("alertas","É necessário informar o campo cep", "danger" );
    if (data.status == -1)
        showMessage("alertas","É necessário informar o campo status", "danger" );
    $.ajax({
        url: "controller/app.php/imovel",
        type: "POST",
        data: data,
        dataType: 'json'
    }).done(function(response){
        if(response)
            showMessage("alertas","Cadastro efetuado com sucesso!", "success" );
    }); 
} 
function getImoveis(){
    $('#imoveis').DataTable().destroy();
    $("#dtBody").html("");
    $.ajax({
        url: "controller/app.php/imovel",
        type: "GET",
        async: true
    }).done(function(response) {
        $.each(response, function (key, value) {
            $("#dtBody")
            .append(
                "<tr>\
                    <td>"+value.cod+"</td>\
                    <td>"+value.endereco+"</td>\
                    <td>"+value.bairro+"</td>\
                    <td>"+value.cidade+"</td>\
                    <td>"+value.estado+"</td>\
                    <td>"+value.cep+"</td>\
                    <td>"+value.status+"</td>\
                    <td><a href='#' onclick='excluirImovel("+value.id+")' data-toggle='modal' data-target='#modal'>\
                            <i class='fas fa-trash-alt'></i>\
                         </a> \
                         <a href='#' onclick='editarImovel("+value.id+")' data-toggle='modal' data-target='#modalEdit'> \
                            <i class='fas fa-edit'></i>\
                        </a>\
                    </td>\
                </tr>"
            );
        });
        $('#imoveis').DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'print',
                    messageTop: "Relatório de Imóveis",
                    exportOptions:{
                        columns: [0,1,2,3,4,5]
                    }
                },
                {
                    extend: 'excelHtml5',
                    messageTop: "Relatório de Imóveis",
                    exportOptions:{
                        columns: [0,1,2,3,4,5]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    messageTop: "Relatório de Imóveis",
                    exportOptions:{
                        columns: [0,1,2,3,4,5]
                    }
                }
            ],
            language:{
                url : "view/js/translate.json"
            }    
        });

    });
}
function excluirImovel(id){
    $("#modalEditLabel").text("Exlusão de Registro");
    $("#modalBody").html("<p>Voce realmente deseja excluir este registro? </p>");
    $("#modalFooter")
    .html('<button type="button" class="btn btn-outline-dark" onclick="exclui('+id+')">Sim</button> <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Não</button>');
    $("#modal").modal("show");
   
}
function exclui(id){
    $.ajax({
        url: "controller/app.php/imovel/"+id,
        type: "DELETE",
        dataType: 'json'
    }).done(function(response){
        if(response)
            showMessage("alertas","Deleção efetuada com sucesso", "success" );
    }); 
    $("#modal").modal("hide");
    getImoveis();
}
function editarImovel(id){
    $("#modalFooter").html("");
    $("#modalContent").css("width", "850px");
    $("#modalEditLabel").text("Editar Registro");
    $("#modalBody").load('view/public/cadImovel.html');
    $.ajax({
        url: "controller/app.php/imovel/"+id,
        type: "GET",
        async: true
    }).done(function(response) {
        $.each(response, function (key, value) {
            $("#endereco").val(value.endereco);
            $("#cod").val(value.cod);
            $("#bairro").val(value.bairro);
            $("#estado option[value="+value.estado_id+"]").attr("selected", true);
            fillCidade();
            $("#cidade option[value="+value.cidade_id+"]").attr("selected", true);
            $("#cep").val(value.cep);
            $("#complemento").val(value.complemento);
            $("#referencia").val(value.referencia);
            $("#status option:selected").text(value.status);
            $("#sabesp").prop("checked", (value.sabesp == "1") ? true :false );
            $("#elektro").prop("checked", (value.elektro  == "1") ? true :false);
            $("#iptu").prop("checked", (value.iptu  == "1") ? true :false);
            $("#btnSaveEdit").attr("onClick", "Editar("+value.id+")");
        });
        $("#h1Cad").remove();
        $("#hrCad").remove();
    });
    $("#modal").modal("show");
}
function Editar(id){
    var data = {
        id: id,
        cod: $("#cod").val(),
        endereco: $("#endereco").val(),
        bairro: $("#bairro").val(),
        cidade_id: $("#cidade").val(),
        cep: $("#cep").val(),
        complemento: $("#complemento").val(),
        referencia: $("#referencia").val(),
        status: $("#status option:selected").text(),
        sabesp: $("#sabesp").is(":checked"),
        elektro: $("#elektro").is(":checked"),
        iptu: $("#iptu").is(":checked")
    }
    if (data.cod == '')
        showMessage("alertas","É necessário informar o campo Cod", "danger" );
    if (data.endereco == '')
        showMessage("alertas","É necessário informar o campo endereco", "danger" );
    if (data.bairro == '')
        showMessage("alertas","É necessário informar o campo bairro", "danger" );
    if (data.cidade == -1)
        showMessage("alertas","É necessário informar o campo cidade", "danger" );
    if (data.cep == '')
        showMessage("alertas","É necessário informar o campo cep", "danger" );
    if (data.status == -1)
        showMessage("alertas","É necessário informar o campo status", "danger" );
    $.ajax({
        url: "controller/app.php/imovel/"+id,
        type: "PUT",
        data: data,
        dataType: 'json'
    }).done(function(response){
        if(response)
            showMessage("alertas","Edição efetuada com sucesso!", "success" );
    }); 
    $("#modal").modal("hide");
    getImoveis();
}

