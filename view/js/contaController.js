
/**
 * Função para salvar os imoveis
 */
function salvar(){
    var data = {
        valor: $("#valor").val(),
        dataPagamento: $("#dtPagamento").val(),
        dataVencimento: $("#dtVencimento").val(),
        imovel_id: $("#imovel_id").val(),
        status: $("#status option:selected").text().toUpperCase(),
        tipo_id: $("#tipo_id").val(),
        descricao: $("#descricao").val()
    }
    if (data.valor == '')
        showMessage("alertas","É necessário informar o campo valor", "danger" );
    if (data.dataPagamento == '')
        showMessage("alertas","É necessário informar o campo data de pagamento", "danger" );
    if (data.dataVencimento == '')
        showMessage("alertas","É necessário informar o campo data de vencimento", "danger" );
    $.ajax({
        url: "controller/app.php/conta",
        type: "POST",
        data: data,
        dataType: 'json'
    }).done(function(response){
        if(response)
            showMessage("alertas","Cadastro efetuado com sucesso!", "success" );
    }); 
} 
function fillTipo(){
    $.ajax({
        url: 'controller/app.php/tipo',
        type: "GET",
        async: false
    }).done(function(response) {
        var selectbox = $("#tipo_id");
        $.each(response, function (i, d) {
            $('<option>').val(d.id).text(d.nome).appendTo(selectbox);
        });
    });
}
function fillImovel(){
    $.ajax({
        url: 'controller/app.php/imovel',
        type: "GET",
        async: false
    }).done(function(response) {
        var selectbox = $("#imovel_id"); 
        $.each(response, function (i, d) {
            $('<option>').val(d.id).text(d.cod).appendTo(selectbox);
        });
    });
}
function getContas(){
    $('#conta').DataTable().destroy();
    $("#dtBody").html("");
    $.ajax({
        url: "controller/app.php/conta",
        type: "GET",
        async: false
    }).done(function(response) {
        $.each(response, function (key, value) {
            $("#dtBody")
            .append(
                "<tr>\
                    <td>"+value.id+"</td>\
                    <td>"+value.descricao+"</td>\
                    <td>R$ "+value.valor+"</td>\
                    <td>"+value.dataVencimento+"</td>\
                    <td>"+value.dataPagamento+"</td>\
                    <td>"+value.status+"</td>\
                    <td>"+value.tipo_id+"</td>\
                    <td>"+value.imovel_id+"</td>\
                    <td><a href='#' onclick='excluirConta("+value.id+")' data-toggle='modal' data-target='#modal'>\
                            <i class='fas fa-trash-alt'></i>\
                         </a> \
                         <a href='#' onclick='editarContas("+value.id+")' data-toggle='modal' data-target='#modalEdit'> \
                            <i class='fas fa-edit'></i>\
                        </a>\
                    </td>\
                </tr>"
            );
        });
        $('#conta').DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'print',
                    messageTop: "Relatório de Inquilinos",
                    exportOptions:{
                        columns: [0,1,2,3,4,5,6]
                    }
                },
                {
                    extend: 'excelHtml5',
                    messageTop: "Relatório de Inquilinos",
                    exportOptions:{
                        columns: [0,1,2,3,4,5,6]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    messageTop: "Relatório de Inquilinos",
                    exportOptions:{
                        columns: [0,1,2,3,4,5,6]
                    }
                }
            ],
            language:{
                url : "view/js/translate.json"
            }     
        });
    });    
}
function excluirConta(id){
    $("#modalEditLabel").text("Exlusão de Registro");
    $("#modalBody").html("<p>Voce realmente deseja excluir este registro? </p>");
    $("#modalFooter")
    .html('<button type="button" class="btn btn-outline-dark" onclick="exclui('+id+')">Sim</button> <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Não</button>');
    $("#modal").modal("show");
   
}
function exclui(id){
    $.ajax({
        url: "controller/app.php/conta/"+id,
        type: "DELETE",
        async: false,
        dataType: 'json'
    }).done(function(response){
        if(response)
            showMessage("alertas","Deleção efetuada com sucesso", "success" );
    }); 
    $("#modal").modal("hide");
    getContas();
}
function editarContas(id){
    $("#modalFooter").html("");
    $("#modalContent").css("width", "850px");
    $("#modalEditLabel").text("Editar Registro");
    $("#modalBody").load('view/public/cadContas.html');
    fillTipo();
    fillImovel();
    $.ajax({
        url: "controller/app.php/conta/"+id,
        type: "GET",
        async: true
    }).done(function(response) {
        $.each(response, function (key, value) {
            $("#valor").val(value.valor);
            $("#dtPagamento").val(value.dataPagamento);
            $("#dtVencimento").val(value.dataVencimento);
            $("#status ").text(value.status),
            $("#imovel_id option[value="+value.imovel_id+"]").attr("selected", true);
            $("#tipo_id option[value="+value.tipo_id+"]").attr("selected", true);
            $("#descricao").val(value.descricao);
            $("#btnSaveEdit").attr("onC\lick", "Editar("+value.id+")");
        });
        $("#h1Cad").remove();
        $("#hrCad").remove();
    });    
    $("#modal").modal("show");
}
function Editar(id){
    var data = {
        valor: $("#valor").val(),
        dataPagamento: $("#dtPagamento").val(),
        dataVencimento: $("#dtVencimento").val(),
        imovel_id: $("#imovel_id").val(),
        tipo_id: $("#tipo_id").val(),
        descricao: $("#descricao").val()
    }
    if (data.cod == '')
        showMessage("alertas","É necessário informar o campo Cod", "danger" );
    if (data.endereco == '')
        showMessage("alertas","É necessário informar o campo endereco", "danger" );
    if (data.bairro == '')
        showMessage("alertas","É necessário informar o campo bairro", "danger" );
    if (data.cidade == -1)
        showMessage("alertas","É necessário informar o campo cidade", "danger" );
    if (data.cep == '')
        showMessage("alertas","É necessário informar o campo cep", "danger" );
    if (data.status == -1)
        showMessage("alertas","É necessário informar o campo status", "danger" );
    $.ajax({
        url: "controller/app.php/conta/"+id,
        type: "PUT",
        data: data,
        dataType: 'json'
    }).done(function(response){
        if(response)
            showMessage("alertas","Edição efetuada com sucesso!", "success" );
    }); 
    $("#modal").modal("hide");
    getContas();
}