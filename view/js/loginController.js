
function logar(){
    data = {
        login: $("#login").val(),
        senha: $("#password").val()
    }
    
    $(function(){
       
        $.ajax({
            url: "controller/app.php/login",
            type: "POST",
            data: data,
            dataType: 'json'
        }).done(function(response){
            if (response == false){
                showMessage("message","Usuário ou senha incorretos", "danger" );
            }else{
                localStorage.setItem("Nome", response.Nome);
                $(window.document.location).attr('href', 'home');
            }
        });
        if ($("#login").val()  == "" || $("#password").val() == ""){
            showMessage("message","É necessário informar usuário e senha!", "danger" );
        }
    });
}