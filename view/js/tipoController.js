if (typeof(cons) == undefined){
    console.log("Entrou aqui");
}else{
    getTipos();
}
/**
 * Função para salvar os imoveis
 */
function salvar(){
    var data = {
        nome: $("#nome").val(),
    }
    if (data.tipo == '')
        showMessage("alertas","É necessário informar o campo tipo", "danger" );
   
    $.ajax({
        url: "controller/app.php/tipo",
        type: "POST",
        data: data,
        dataType: 'json'
    }).done(function(response){
        if(response)
            showMessage("alertas","Cadastro efetuado com sucesso!", "success" );
    }); 
} 
function getTipos(){
    $("#dtBody").html("");
    $.get( "controller/app.php/tipo", function( response ) {
        $.each(response, function (key, value) {
            $("#dtBody")
            .append(
                "<tr>\
                    <td>"+value.id+"</td>\
                    <td>"+value.nome+"</td>\
                    <td><a href='#' onclick='excluirTipo("+value.id+")' data-toggle='modal' data-target='#modal'>\
                            <i class='fas fa-trash-alt'></i>\
                         </a> \
                         <a href='#' onclick='editarTipo("+value.id+")' data-toggle='modal' data-target='#modalEdit'> \
                            <i class='fas fa-edit'></i>\
                        </a>\
                    </td>\
                </tr>"
            );
        });
        $("#tipoTables").DataTable(
            {
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                language:{
                    url : "view/js/translate.json"
                }    
            }
        );
    });
}

function excluirTipo(id){
    $("#modalEditLabel").text("Exlusão de Registro");
    $("#modalBody").html("<p>Voce realmente deseja excluir este registro? </p>");
    $("#modalFooter")
    .html('<button type="button" class="btn btn-outline-dark" onclick="exclui('+id+')">Sim</button> <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Não</button>');
    $("#modal").modal("show");
   
}
function exclui(id){
    $.ajax({
        url: "controller/app.php/tipo/"+id,
        type: "DELETE",
        dataType: 'json'
    }).done(function(response){
        if(response.flag)
            showMessage("alertas","Deleção efetuada com sucesso", "success" );
        else
            showMessage("alertas",response.message, "danger" );
    }); 
    $("#modal").modal("hide");
    getImoveis();
}
function editarTipo(id){
    var cons = true;
    $("#modalContent").css("width", "850px");
    $("#modalEditLabel").text("Editar Registro");
    $.get("controller/app.php/tipo/"+id, function( response ) {
        $.each(response, function (key, value) {
            $("#nomeEdit").val(value.nome);
            $("#btnEdit").attr("onClick", "Editar("+value.id+")");
        });
    });
    $("#modal").modal("show");
}
function Editar(id){
    var data = {
        id: id,
        nome: $("#nomeEdit").val(),
    }
    if (data.cod == '')
        showMessage("alertas","É necessário informar o campo Cod", "danger" );
    if (data.endereco == '')
        showMessage("alertas","É necessário informar o campo endereco", "danger" );
    if (data.bairro == '')
        showMessage("alertas","É necessário informar o campo bairro", "danger" );
    if (data.cidade == -1)
        showMessage("alertas","É necessário informar o campo cidade", "danger" );
    if (data.cep == '')
        showMessage("alertas","É necessário informar o campo cep", "danger" );
    if (data.status == -1)
        showMessage("alertas","É necessário informar o campo status", "danger" );
    $.ajax({
        url: "controller/app.php/tipo/"+id,
        type: "PUT",
        data: data,
        dataType: 'json'
    }).done(function(response){
        if(response){
            showMessage("alertas","Edição efetuada com sucesso!", "success" );
        }
        console.log(response);
        getTipos();
        }); 
        $("#modal").modal("hide");
}

