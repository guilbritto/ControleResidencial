function showMessage(classname, message, type){
    $("#"+classname).html(
         "<div class='alert alert-"+type+" alert-dismissible fade show' role='alert'>\
         "+message+"\
         <button type='button' class='close' data-dismiss='alert' aria-label='Close'>\
             <span aria-hidden='true'>&times;</span>\
         </button>\
     </div> ");
    $("#"+classname).fadeTo(2000, 1000).slideUp(1000, function(){
        $("#"+classname).slideUp(1000);
    });
}